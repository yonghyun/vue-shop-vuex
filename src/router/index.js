import Vue from 'vue'
import Router from 'vue-router'

import Home from '../components/Home'
import ShoppingCart from '../components/ShoppingCart'
import Me from '../components/Me'
import Category from '../components/Category'
import BookDetail from '../components/books/BookDetail'

Vue.use(Router)

export default new Router({
  linkActiveClass: 'active',
  routes: [
    {
      name: 'Home',
      path: '/',
      component: Home
    },
    {
      name: 'BookDetail',
      path: '/bookdetail/:id',
      component: BookDetail
    },
    {
      name: 'ShoppingCart',
      path: '/shoppingcart',
      component: ShoppingCart
    },
    {
      name: 'Me',
      path: '/me',
      component: Me
    },
    {
      name: 'Category',
      path: '/category',
      component: Category
    }
  ]
})
