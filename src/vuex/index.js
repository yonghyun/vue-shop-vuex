import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
import books from './modules/books'
// 声明引入此库文件的Vue实例使用Vuex插件应用状态管理
Vue.use(Vuex)
// 从环境变量判断当前的运行模式
const debug = process.env.NODE_ENV !== 'production'

// 导出store实例对象
export default new Vuex.Store({
  strict: debug, // 设置运行模式,true为调试模式，false为生产模式
  plugin: debug ? [createLogger()] : [], // 插件
  // state: {state}, // Vuex store实例跟状态对象，定义共享的状态变量，就像vue实例中的data
  // actions: {actions}, // 动作，向store发出调用通知，执行本地或者远端的某一个操作，可理解为store的methods
  // getters: {getters}, // 读取器，外部程序通过它读取变量的具体值，可理解为store的计算属性
  // mutations: {mutations}, // 修改器，修改state中定义的变量
  modules: {books} // 模块，向store注入其他子模块
})
