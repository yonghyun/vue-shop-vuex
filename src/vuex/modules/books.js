// import Vue from 'vue'
import FakeData from '../../data/data'
// state相当于stores实例的data
const state = {
  slides: [],
  announcement: '',
  lastUpdated: [],
  recommended: []
}

// getters相当于store实例的computed
const getters = {
  announcement: state => state.announcement,
  slides: state => state.slides,
  lastUpdated: state => state.lastUpdated,
  recommended: state => state.recommended,
  totalSlides: state => state.slides.length
}

// actions相当于store的methods，state中的内容只能通过Mutation来修改
// 类似MVC模式中的Controller，下一步交给Mutation进行处理
const actions = {
  getStarted (context) {
    // Vue.http.get('/api/get-start', (res) => {
    //   context.commit('startedDataReceived', res.body)
    // })
    context.commit('startedDataReceived', FakeData.getHomeData())
  }
}

const mutations = {
  startedDataReceived (state, startedData) {
    state.announcement = startedData.announcement
    state.slides = startedData.slides
    state.lastUpdated = startedData.lastUpdated
    state.recommended = startedData.recommended
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
